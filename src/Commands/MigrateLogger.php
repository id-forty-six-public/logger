<?php

namespace idfortysix\logger\Commands;

use Illuminate\Console\Command;
use Artisan;


class MigrateLogger extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migratelogger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'makes logger migrations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		Artisan::call('migrate', ['--path' => '/vendor/id-forty-six/logger/src/migrations/', '--database' => 'logger', '--force' => true]);
        $this->info("OK");
    }
}