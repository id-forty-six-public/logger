<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('logger')->create('log', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('app')->collation('ascii_general_ci');
            $table->string('type')->collation('ascii_general_ci');
            $table->string('message')->collation('utf8_general_ci');
            $table->string('clientID')->collation('ascii_general_ci')->nullable();
            $table->integer('quantity')->unsigned()->nullable();
            $table->longtext('backtrace')->collation('ascii_general_ci')->nullable();
            $table->json('variables')->nullable();
            
            $table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('logger')->dropIfExists('log');
    }
}