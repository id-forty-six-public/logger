<?php

namespace idfortysix\logger;

use Illuminate\Support\ServiceProvider;
use idfortysix\logger\Commands\MigrateLogger;

class LoggerServiceProvider extends ServiceProvider
{
    
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                MigrateLogger::class,
            ]);
    	}
    }

    public function register()
    {
         
    }
}