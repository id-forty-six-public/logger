<?php

namespace idfortysix\logger;

use Moloquent;

//class Log extends Model
class Log extends Moloquent
{
    protected $connection = 'logger';
    protected $collection = 'log';
    protected $fillable = ['type', 'message', 'clientID', 'quantity', 'variables'];

    public static function create(Array $data)
    {
        try
        {
            $log = new self;
            $log->fill($data);

            if ($data['backtrace'])
            {
                $log->backtrace = $log->formatBacktrace($data['backtrace']);
            }
            $log->app = strtolower(env('APP_NAME'));
            
            $log->save();
        }
        catch(\Exception $e)
        {
            //
        }
        
        return $log;
    }

    private function formatBacktrace($backtrace)
    {
        if(is_array($backtrace) && array_key_exists('class', $backtrace) && array_key_exists('function', $backtrace))
        {
            return $backtrace['class']."::".$backtrace['function'];
        }
        elseif(is_array($backtrace))
        {
            return print_r($backtrace, true);
        }
        elseif(is_string($backtrace))
        {
            return $backtrace;
        }
    }
}