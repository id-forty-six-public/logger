Logger app
===

## About

This package enables custom logging to MYSQL db.

## Install

Download package
```
./composer.phar require id-forty-six/logger
```

If docker instance doesn't have Mysql

```
apt-get install php7.0-mysql
```

Uncomment line in php.ini 

```
extension=php_pdo_mysql.dll
```
Restart driver
```
/etc/init.d/php7.0-fpm restart
```
Add to config->app.php
```
idfortysix\logger\LoggerServiceProvider::class,
```

Add to config->database.php
```
 'mysqllogger' => [
    'driver'   => 'mysql',
    'host'     => env('DB_HOST3', '172.19.0.33'),
    'port'     => env('DB_PORT3', 3306),
    'database' => env('DB_DATABASE3', 'logger'),
    'username' => env('DB_USERNAME3', 'local'),
    'password' => env('DB_PASSWORD3', 'local'),
],
```

## Commands

Migrate will create table in configured Mysql DB
```
./artisan migratelogger
```

## Usage

use idfortysix\logger\Log;
```
Log::create([
    'message'   => 'unsubscribe url',                //required
    'type'      => 'user_http',                      //required || in:'user_http', 'driver', ... 
    'backtrace' => debug_backtrace()[0],             //shows class and method error occured 
    'clientID'  => $clientID,                        //clientID
    'quantity'  => 1000,                             //default=1 current progress 
    'variables' => json_encode(['test'=>'test'])     //additional info in json format
]);
```
## Types

Billing
```
subscription_error - braintree subscriptiono metu ivykes erroras
subscription_pos - pozityvus braintree webhook (pvz.: sekmingai nucharginta ar pan)
subscription_neg - negatyvus braintree webhook (pvz.: cancelintas subscription ar pan)

manualsub_error - manual subscriptiono (Coingate, Paysera) metu ivykes erroras
manualsub_pos - pozityvus manualsub webhook (pvz.: sekmingas paymentas)

plan_swap - automatiskai pakeistas vartotojo planas

```

Webapp
```
campaign_stop - sustabdyti vartotojo campaignai

manualsub_error - manual subscriptiono (Coingate, Paysera) metu ivykes erroras

driver - pvz guzzle requesto metu ivykes error

```
Sendapi
```
user_http - vartotojas pateko i bloga linka

driver - pvz guzzle requesto metu ivykes error

```
            